import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import request from 'superagent';
import {FusePageSimple, DemoContent} from '@fuse';
import Dropzone from 'react-dropzone';
import { LinearProgress } from '@material-ui/core';
import axios from 'axios';

const styles = theme => ({
    layoutRoot: {}
});

class Basic extends Component {
  constructor() {
    super();
    this.onDrop = async (files) => {
        const response = await axios.post('https://api.qa.np.1shift.io/shipment/shipment_files/1/upload',  
        {
          headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        }
      },{
                count:1,
                content_type:"jpg"
          })
    
                
      var options = {
        crossdomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        }
      };
      try{
        var fd = new FormData()
        fd.append(files[0].name, files[0]);
        console.log('aaaa', fd)
        const a = await axios.put(response.data[0].file_url, fd, options);
        console.log({a})
      } catch(err) {
        console.log(err)
      }
      this.setState({files})
    };
    this.state = {
      files: []
    };
  }

  render() {
    const files = this.state.files.map(file => (
      <li key={file.name}>
        {file.name} - {file.size} bytes
      </li>
    ));

    return (
        <div>
            <Dropzone onDrop={this.onDrop}>
              {({getRootProps, getInputProps}) => (
                <section className="container">
                  <div {...getRootProps({className: 'dropzone'})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                  </div>
                  <aside>
                    <h4>Files</h4>
                    <ul>{files}</ul>
                  </aside>
                </section>
              )}
            </Dropzone>
            <LinearProgress color="secondary" variant="determinate" value={45} />
        </div>
    );
  }
}

export default withStyles(styles, {withTheme: true})(Basic);